import Notification from "./AbstractNotification"

export const MAX_SHOW_NOTIFICATION = 3
export const Queue: Notification[] = []
export const Notifications: Notification[] = []
