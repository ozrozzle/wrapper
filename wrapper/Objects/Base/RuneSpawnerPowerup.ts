import { WrapperClass } from "../../Decorators"
import Entity from "./Entity"

@WrapperClass("C_DOTA_Item_RuneSpawner_Powerup")
export default class RuneSpawnerPowerup extends Entity {
}
