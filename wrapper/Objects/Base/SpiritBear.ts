import { WrapperClass } from "../../Decorators"
import Unit from "./Unit"

@WrapperClass("C_DOTA_Unit_SpiritBear")
export default class SpiritBear extends Unit {
}
