import { WrapperClass } from "../../Decorators"
import Hero from "../Base/Hero"

@WrapperClass("C_DOTA_Unit_Hero_FacelessVoid")
export default class npc_dota_hero_faceless_void extends Hero {
}
